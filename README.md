# ADACS Django Template

![django]![tailwindcss]

A production ready django 4.2 template for jump-starting ADACS web applications. Powered by [Cookiecutter](https://github.com/cookiecutter/cookiecutter). Based off of the awesome [cookiecutter-django](https://github.com/cookiecutter/cookiecutter-django/) template and snippets from the excellent [wemake django template](https://github.com/wemake-services/wemake-django-template).

![home-page]

## Features

- Django 4.2
- Works with Python 3.12
- [Tailwind CSS](https://tailwindcss.com/docs) & [dasiyUI](https://daisyui.com)
- [Poetry](https://github.com/python-poetry/poetry) for managing dependencies
- Optimized development and production settings
- Registration via [django-allauth](https://github.com/pennersr/django-allauth)
- Comes with custom user model ready to go
- Docker support using [docker-compose](https://github.com/docker/compose) for development and production

## Optional Integrations

_These features can be enabled during initial project setup._

- [Django Rest Framework API](https://www.django-rest-framework.org/)
- [HTMX](https://www.htmx.org/)

## Constraints

- Only maintained 3rd party libraries are used.
- MySQL 8+.
- Environment variables for configuration.
- Highly opinionated :D.

## Usage

To get started with a new django app based on this template you'll
need to install three dependencies:

1. [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/).
2. [Poetry](https://python-poetry.org/docs/)
3. [MySQL](https://dev.mysql.com/doc/refman/9.0/en/installing.html)

### Install Cookiecutter

The recommended way to install Cookiecutter is via [`pipx`](https://github.com/pypa/pipx):

```bash
pipx install cookiecutter
```

Or via global `pip`:

```bash
pip install cookiecutter
```

### Install Poetry

For Linux, macOS, Windows (WSL)

```sh
curl -sSL https://install.python-poetry.org | python3 -
```

For Windows (PowerShell)

```sh
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

### Install MySQL

Make sure you have MySQL 8+ installed ([installation instructions](https://dev.mysql.com/doc/mysql-installation-excerpt/9.0/en/)):

Ubuntu

```bash
sudo apt install mysql-server
```

MacOS

```bash
brew install mysql
```

## Run the Cookiecutter template

Make sure you have all the dependencies installed, then, create a new project!

```bash
cookiecutter https://gitlab.com/CAS-eResearch/internal/adacs-django-template/
```

<!-- Markdown Links --->

[django]: https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=white
[tailwindCSS]: https://img.shields.io/badge/TailwindCSS-06B6D4?style=for-the-badge&logo=TailwindCSS&logoColor=white
[home-page]: ./{{cookiecutter.project_name}}/src/apps/static/images/Cute_pythons.jpg
