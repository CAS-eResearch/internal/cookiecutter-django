"""
This module is called after project is created.

It does the following:
1. Generates and saves random secret key
2. Installs poetry dependecies
3. Installs tailwind
4. Initialises a git repo and makes an initial commit
5. Prints further instructions

A portion of this code was adopted from Django's standard crypto functions and
utilities, specifically:
https://github.com/django/django/blob/master/django/utils/crypto.py

And from pydanny's cookiecutter-django:
https://github.com/pydanny/cookiecutter-django

And from wemake services cookiecutter-django
https://github.com/wemake-services/wemake-django-template

"""

import os
import secrets
import shutil
import string
import subprocess

# CHANGEME mark
CHANGEME = "__CHANGEME__"

# Get the root project directory
PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
PROJECT_NAME = "{{ cookiecutter.project_name }}"

# Messages
PROJECT_SUCCESS = """
{0} has been created.
Now you can start working on it

    cd {0}

The first thing you'll need to do is setup a mysql database (version 8+) and
then add the details to the {0}/.env file.

To start the tailwind file watcher

    cd {0}/src/
    poetry run python manage.py tailwind start

To start the development server

    cd {0}/src
    poetry run python manage.py migrate
    poetry run python manage.py runserver

Get coding!

"""


def _get_random_string(length=50):
    """
    Returns a securely generated random string.

    The default length of 12 with the a-z, A-Z, 0-9 character set returns
    a 71-bit value. log_2((26+26+10)^12) =~ 71 bits

    >>> secret = _get_random_string()
    >>> len(secret)
    50

    """
    punctuation = (
        string.punctuation.replace(
            '"',
            "",
        )
        .replace(
            "'",
            "",
        )
        .replace(
            "\\",
            "",
        )
        .replace(
            "$",
            "",  # see issue-271
        )
    )

    chars = string.digits + string.ascii_letters + punctuation
    return "".join(secrets.choice(chars) for _ in range(length))


def _create_secret_key(config_path):
    # Generate a SECRET_KEY that matches the Django standard
    secret_key = _get_random_string()

    with open(config_path, "r+") as config_file:
        # Replace CHANGEME with SECRET_KEY
        file_contents = config_file.read().replace(CHANGEME, secret_key, 1)

        # Write the results to the file:
        config_file.seek(0)
        config_file.write(file_contents)
        config_file.truncate()


def print_futher_instuctions():
    """Shows user what to do next after project creation."""
    print(PROJECT_SUCCESS.format(PROJECT_NAME))  # noqa: WPS421


def copy_local_configuration():
    """
    Handler to copy local configuration.

    It is copied from ``.template`` files to the actual files.
    """
    secret_template = os.path.join(
        PROJECT_DIRECTORY,
        ".env.template",
    )
    secret_config = os.path.join(
        PROJECT_DIRECTORY,
        ".env",
    )
    shutil.copyfile(secret_template, secret_config)
    _create_secret_key(secret_config)


def remove_django_rest_framework_starter_files():
    os.remove(os.path.join("src", "config", "api_router.py"))
    shutil.rmtree(os.path.join("src", "apps", "users", "api"))
    os.remove(os.path.join("src", "apps", "users", "tests", "test_drf_urls.py"))
    os.remove(os.path.join("src", "apps", "users", "tests", "test_drf_views.py"))
    os.remove(os.path.join("src", "apps", "users", "tests", "test_swagger.py"))


def remove_custom_user_manager_files():
    os.remove(
        os.path.join(
            "src",
            "apps",
            "users",
            "tests",
            "test_managers.py",
        )
    )


def setup_git():
    print("Setting up new git repository...")
    subprocess.run(["git", "init"])
    subprocess.run(["git", "add", "*"])
    subprocess.run(["git", "commit", "-m", '"Initial commit"'])


def install_poetry_dependencies():
    try:
        print("Installing project dependencies...")
        subprocess.run(["poetry", "install"])
    except Exception as e:
        print(
            f"""
        Installing dependencies with poetry failed with exception: {e}.

        To install poetry: https://python-poetry.org/docs/#installation
        """
        )
        exit(1)


def install_tailwind():
    print("Installing tailwind...")
    # os.chdir("src/")
    subprocess.run(
        ["poetry", "run", "python", "manage.py", "tailwind", "install"], cwd="src/"
    )


copy_local_configuration()

if {{cookiecutter.username_type == "username"}}:
    remove_custom_user_manager_files()

if not {{cookiecutter.use_django_rest_framework}}:
    remove_django_rest_framework_starter_files()

if {{cookiecutter.run_poetry}}:
    install_poetry_dependencies()
    install_tailwind()

if {{cookiecutter.setup_git}}:
    setup_git()

print_futher_instuctions()
