import logging
from http import HTTPStatus

from django.test import Client, TestCase
from django.urls import reverse

from apps.users.tests.factories import UserFactory


class TestSwagger(TestCase):

    def setUp(self):
        self.user = UserFactory()

    def create_admin_client(self):
        self.user.is_staff = True
        self.user.is_superuser = True
        self.user.is_active = True
        self.user.save()
        client = Client()
        client.login(username=self.user.username, password=self.password)
        client.login(email=self.email, password=self.password)
        return client

    def test_swagger_accessible_by_admin(self):
        admin_client = self.create_admin_client()
        url = reverse("api-docs")
        response = admin_client.get(url)
        assert response.status_code == HTTPStatus.OK

    def test_swagger_ui_not_accessible_by_normal_user(self):
        client = Client()
        # Stop test from outputting swagger warning logs.
        logging.disable(logging.CRITICAL)
        url = reverse("api-docs")
        response = client.get(url)
        assert response.status_code == HTTPStatus.FORBIDDEN

    def test_api_schema_generated_successfully(self):
        admin_client = self.create_admin_client()
        url = reverse("api-schema")
        response = admin_client.get(url)
        assert response.status_code == HTTPStatus.OK
