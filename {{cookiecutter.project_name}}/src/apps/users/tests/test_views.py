from http import HTTPStatus

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import AnonymousUser
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpRequest, HttpResponseRedirect
from django.test import RequestFactory, TestCase
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from apps.users.forms import UserAdminChangeForm
from apps.users.models import User
from apps.users.tests.factories import UserFactory
from apps.users.views import UserRedirectView, UserUpdateView, user_detail_view


class TestUserUpdateView(TestCase):

    def setUp(self):
        self.rf = RequestFactory()
        self.user = UserFactory()

    def dummy_get_response(self, request):
        return None

    def test_get_success_url(self):
        """
        Each user should have an associated user url that uses their username.
        """
        view = UserUpdateView()
        request = self.rf.get("/fake-url/")
        request.user = self.user

        view.request = request
        assert view.get_success_url() == f"/users/{self.user.username}/"

    def test_get_object(self):
        """
        Make sure that UserUpdateView returns a User Object."
        """
        view = UserUpdateView()
        request = self.rf.get("/fake-url/")
        request.user = self.user

        view.request = request

        assert view.get_object() == self.user

    def test_form_valid(self):
        """
        Test that you can submit the update user form.
        """
        view = UserUpdateView()
        request = self.rf.get("/fake-url/")

        # Add the session/message middleware to the request
        SessionMiddleware(self.dummy_get_response).process_request(request)
        MessageMiddleware(self.dummy_get_response).process_request(request)
        request.user = self.user

        view.request = request

        # Initialize the form
        form = UserAdminChangeForm()
        form.cleaned_data = {}
        form.instance = self.user
        view.form_valid(form)

        messages_sent = [m.message for m in messages.get_messages(request)]
        assert messages_sent == [_("Information successfully updated")]


class TestUserRedirectView(TestCase):

    def setUp(self):
        self.rf = RequestFactory()
        self.user = UserFactory()

    def test_get_redirect_url(self):
        view = UserRedirectView()
        request = self.rf.get("/fake-url")
        request.user = self.user

        view.request = request
        assert view.get_redirect_url() == f"/users/{self.user.username}/"


class TestUserDetailView(TestCase):
    def setUp(self):
        self.rf = RequestFactory()
        self.user = UserFactory()

    def test_authenticated(self):
        """
        System must return valid response to authenticated user.
        """
        request = self.rf.get("/fake-url/")
        request.user = UserFactory()
        response = user_detail_view(request, username=self.user.username)

        assert response.status_code == HTTPStatus.OK

    def test_not_authenticated(self):
        request = self.rf.get("/fake-url/")
        request.user = AnonymousUser()
        response = user_detail_view(request, username=self.user.username)
        login_url = reverse(settings.LOGIN_URL)

        assert isinstance(response, HttpResponseRedirect)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url == f"{login_url}?next=/fake-url/"
