from django.test import TestCase
from rest_framework.test import APIRequestFactory

from apps.users.api.views import UserViewSet
from apps.users.models import User
from apps.users.tests.factories import UserFactory


class TestUserViewSet(TestCase):

    def setUp(self):
        self.api_rf = APIRequestFactory()
        self.user = UserFactory()

    def test_get_queryset(self):
        view = UserViewSet()
        request = self.api_rf.get("/fake-url/")
        request.user = self.user

        view.request = request

        assert self.user in view.get_queryset()

    def test_me(self):
        view = UserViewSet()
        request = self.api_rf.get("/fake-url/")
        request.user = self.user

        view.request = request

        response = view.me(request)  # type: ignore[call-arg, arg-type, misc]

        assert response.data == {
            "username": self.user.username,
            "url": f"http://testserver/api/users/{self.user.username}/",
            "name": self.user.name,
        }
