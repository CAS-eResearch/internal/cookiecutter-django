import contextlib
from http import HTTPStatus
from importlib import reload

from django.contrib import admin
from django.contrib.auth import settings
from django.contrib.auth.models import AnonymousUser
from django.test import Client, RequestFactory, TestCase
from django.urls import reverse

from apps.users.models import User
from apps.users.tests.factories import UserFactory


class TestUserAdmin(TestCase):

    def setUp(self):
        self.rf = RequestFactory()
        self.user = UserFactory()

    def get_admin_client(self):
        self.username = "test_admin"
        self.password = "My_R@ndom_P@ssw0rd"
        user, _ = User.objects.get_or_create(username=self.username)
        user.set_password(self.password)
        user.is_staff = True
        user.is_superuser = True
        user.is_active = True
        user.save()
        self.user = user

        client = Client()
        client.login(username=self.username, password=self.password)

        return client

    def test_changelist(self):
        admin_client = self.get_admin_client()
        url = reverse("admin:users_user_changelist")
        response = admin_client.get(url)
        assert response.status_code == HTTPStatus.OK

    def test_search(self):
        admin_client = self.get_admin_client()
        url = reverse("admin:users_user_changelist")
        response = admin_client.get(url, data={"q": "test"})
        assert response.status_code == HTTPStatus.OK

    def test_add(self):
        admin_client = self.get_admin_client()
        url = reverse("admin:users_user_add")
        response = admin_client.get(url)
        assert response.status_code == HTTPStatus.OK

        response = admin_client.post(
            url,
            data={
                "username": "test",
                "password1": "My_R@ndom-P@ssw0rd",
                "password2": "My_R@ndom-P@ssw0rd",
            },
        )
        assert response.status_code == HTTPStatus.FOUND
        assert User.objects.filter(username="test").exists()

    def test_view_user(self):
        admin_client = self.get_admin_client()
        user = User.objects.get(username="test_admin")
        url = reverse("admin:users_user_change", kwargs={"object_id": user.pk})
        response = admin_client.get(url)
        assert response.status_code == HTTPStatus.OK

    def _force_allauth(self):
        settings.DJANGO_ADMIN_FORCE_ALLAUTH = True
        # Reload the admin module to apply the setting change
        import apps.users.admin as users_admin

        with contextlib.suppress(admin.sites.AlreadyRegistered):  # type: ignore[attr-defined]
            reload(users_admin)

    def test_allauth_login(self):
        request = self.rf.get("/fake-url")
        request.user = AnonymousUser()
        response = admin.site.login(request)

        # The `admin` login view should redirect to the `allauth` login view
        target_url = reverse(settings.LOGIN_URL) + "?next=" + request.path
        self.assertRedirects(response, target_url, fetch_redirect_response=False)
